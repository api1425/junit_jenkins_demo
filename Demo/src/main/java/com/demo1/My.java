package com.demo1;

public class My {

	public int add(int a, int b) {
		return a+b;
	}
	
	public boolean isPrime(int n) {
		
		for(int i=2;i<=Math.sqrt(n);i++) {
			if(n%i==0)
				return false;
			
		}
		
		return true;
	}
	
	public boolean isEven(int n) {
		if(n%2==0)
			return true;
		return false;
	}
	

	public int hcf(int a, int b) {
		if (a == 0)
	        return b;
	    return hcf(b % a, a);
	}
	
	public static void main(String args[]) {
		System.out.println(new My().hcf(4,20));
	}
}
