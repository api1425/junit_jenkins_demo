package com.mytestfiles;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.demo1.My;

public class Test1 {
	static My obj;
	
	@BeforeClass
	public static void initClass() {
		System.out.println("Starting executing test cases using Junit4");
		obj=new My();
	}
	
	@Test
	public void addTestCase() {
		Assert.assertEquals(obj.add(1,2), 3);
	}
	
	@Test
	public void isPrimeTestCase() {
		Assert.assertEquals(obj.isPrime(17), true);
	}
	
	@AfterClass
	public static void resetClass() {
		obj=null;
	}
}
